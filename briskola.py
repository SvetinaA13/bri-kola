from igra import *
from tkinter import *
from tkinter import ttk


class Briskola:
    def __init__(self, master):
        # konec igre je True, da stisk na gumb nova igra ne prišteje točke računalniku
        self.konec_igre = True

        # ustvari glavni okvir
        self.frame = ttk.Frame(master, width=448, height=460)
        self.frame.grid(columnspan=11, rowspan=8)
        self.lprazenframe = ttk.Frame(self.frame, width=100, height=450)  # levi okvir
        self.lprazenframe.grid(column=0, row=0, rowspan=6, sticky='n')
        self.dprazenframe = ttk.Frame(self.frame, width=100, height=450)  # desni okvir
        self.dprazenframe.grid(column=7, columnspan=4, row=0, rowspan=6)

# ###########################        spremenljivke         ############################################################
        # kdo je igral katero karto: spremenljivki spreminja metoda igraj razreda Player
        self.human_izbira = IntVar()  # pove kateri indeks v seznam kart v roki je izbral igralec
        self.poberi_var = StringVar()  # ima vrednost 'Poberi' ko mora igralec pobrat karto s kupa
        self.br = False  # prazni spremenljivki, da deluje metoda ustvari
        self.kup = False

        # števci točk obeh igralcev
        self.humantocke_var = IntVar(value=0)
        self.pctocke_var = IntVar(value=0)

        # spremenljivke za karte v igralčevi in nasprotnikovi roki
        self.igralec_roka = [i for i in range(3)]
        self.nasprotnik_roka = [i for i in range(3)]

        # šteje koliko iger je kdo zmagal
        self.human_zmagal = IntVar(value=0)
        self.pc_zmagal = IntVar(value=0)

        # pove kdo je zmagal prejšnjo igro
        self.zmagal = 0

        # omogoča delovanje gumba 'nadaljuj'
        self.nadaljuj = StringVar(value='Nadaljuj')

# #################### sledeči objekti ležijo na sredini okna #########################################################
        # ustvari okvir za igralno polje
        self.igralnopolje = Canvas(self.frame, width=248, height=150)
        self.igralnopolje.grid(column=1, row=1, columnspan=6, rowspan=3)
        self.igrane_karte = list((i for i in range(2)))  # seznam igranih kart obeh igralcev

        # ustvari okvirja za igralca
        self.igralec_can = Canvas(self.frame, width=248, height=150)  # polje za igralca (človek)
        self.igralec_can.grid(column=1, columnspan=6, row=4, rowspan=2, sticky='S')

        self.nasprotnik_can = Canvas(self.frame, width=248, height=150)
        self.nasprotnik_can.grid(column=1, row=0, columnspan=6, sticky='N')

# ################### sledeči objekti ležijo v levem okvirju ##########################################################
        # ustvari gumb za zagon igre
        self.nova_igra = ttk.Button(self.lprazenframe, text='Nova igra', command=lambda: self.ustvari(False, False))

        # ustvari spremenljivko, ki izpiše kdo je na potezi
        self.besedilo_fr = ttk.Frame(self.lprazenframe, width=100, height=150)
        self.besedilo_fr.grid(row=2, rowspan=3)
        self.besedilo = ttk.Label(self.besedilo_fr)
        self.besedilo.grid(column=0, row=0)
        self.napotezi = StringVar(value='')  # napiše kdo je na potezi
        self.izpisipotezo = ttk.Label(self.besedilo_fr, textvariable=self.napotezi, justify=LEFT)
        self.izpisipotezo.grid(column=0, row=1)

        self.nadaljuj_igro = ttk.Button(self.lprazenframe, text='Nadaljuj', command=self.runda)
        self.nadaljuj_igro.bind('<Return>', self.runda)

# ################### sledeči objekti ležijo pod igralnim poljem ######################################################
        self.tocke = ttk.Label(self.frame, text='Točke', anchor=W)
        self.tocke.grid(column=0, row=6)

        self.humantocke_lab = ttk.Label(self.frame, text='Igralec:', anchor=W)
        self.humantocke_lab.grid(column=1, row=6)

        self.pctocke_lab = ttk.Label(self.frame, text='Nasprotnik:', anchor=W)
        self.pctocke_lab.grid(column=3, row=6)

        self.humantocke_vlab = ttk.Label(self.frame, textvariable=self.humantocke_var, anchor=W)
        self.humantocke_vlab.grid(column=2, row=6)

        self.pctocke_vlab = ttk.Label(self.frame, textvariable=self.pctocke_var, anchor=W)
        self.pctocke_vlab.grid(column=4, row=6)

        self.igre = ttk.Label(self.frame, text='Igre', anchor=W)
        self.igre.grid(column=0, row=7)

        self.human_zmagal_lab = ttk.Label(self.frame, text='Igralec:', anchor=W)
        self.human_zmagal_lab.grid(column=1, row=7)

        self.human_zmagal_vlab = Label(self.frame, textvariable=str(self.human_zmagal))
        self.human_zmagal_vlab.grid(column=2, row=7)

        self.pc_zmagal_lab = Label(self.frame, text='Nasprotnik:', anchor=W)
        self.pc_zmagal_lab.grid(column=3, row=7)

        self.pc_zmagal_vlab = Label(self.frame, textvariable=str(self.pc_zmagal), anchor=W)
        self.pc_zmagal_vlab.grid(column=4, row=7)

# #################### sledeči objekti ležijo v desnem okvirju ########################################################
        # ustvari okvirje za kupcek in glavno karto (briskolo)
        self.kupcek = Canvas(self.dprazenframe, width=100, height=150)  # okvir za kupček
        self.kupcek.grid(column=0, row=1)

        self.briskola_can = Canvas(self.dprazenframe, width=100, height=150)
        self.briskola_can.grid(column=0, row=0, sticky='n')

        # pod kupom kart se izpiše 'Poberi', ko mora igralec pobrati karto
        self.poberi_lab = ttk.Label(self.dprazenframe, textvariable=self.poberi_var)

    def ustvari(self, pcc, ponastavi):  # pcc = True pomeni da računalnik igra proti računalniku, sicer proti človeku
        # igra v teku, ni konec
        self.konec_igre = False

        if ponastavi:
            self.human_zmagal.set(0)
            self.pc_zmagal.set(0)

        # postavi gumb za novo igro na polje
        self.nova_igra['command'] = lambda: self.ustvari(pcc, False)
        self.nova_igra.bind('<Button-1>', self.pristej_tocke)
        self.nova_igra.grid(column=0, row=0, sticky='n')

        # ponastavi vse spremenljivke
        if self.br:
            self.briskola_can.delete(self.br)
        if self.kup:
            self.kupcek.delete(self.kup)
        self.poberi_var.set('')
        self.napotezi.set('')
        self.humantocke_var.set(0)
        self.pctocke_var.set(0)

        self.besedilo['text'] = 'Na potezi je'

        # sprazni igralčevo in nasprotnikovo polje
        self.sprazni_kanvas_igralec()
        self.sprazni_kanvas_nasprotnik()

        # sprazni polje
        self.pocisti_polje()

        # v self.igra je shranjena igra
        self.igra = ustvari_igro(p=pcc)
        self.igra.polje = self
        if self.zmagal == 1:
            self.igra.prvi, self.igra.drugi = self.igra.human, self.igra.pc
        if self.zmagal == -1:
            self.igra.prvi, self.igra.drugi = self.igra.pc, self.igra.human
        self.napotezi.set(self.igra.prvi.name)

        self.kup = self.kupcek.create_image(5, 5, image=back, anchor='nw')
        self.br = self.briskola_can.create_image(5, 5, image=self.igra.briskola.image, anchor='nw')

        self.poberi_lab.grid(column=0, row=3)

        if not pcc:
            self.kupcek.bind('<Button-1>', self.igra.human.poberi)
            self.igralec_can.bind('<Button-1>', self.human_igraj)
            if self.nadaljuj.get():
                self.nadaljuj_igro.grid_forget()
        else:
            self.nadaljuj_igro.grid()
            self.poberi_lab.grid_forget()

        # ustvari slike hrbtnih strani kart v nasprotnikovi roki
        self.hrbtna_stran()

        # ustvari slike igralčevih kart
        self.prikazi_karte()

        while bool(self.igra.prvi.roka):
            self.igra.runda()
        self.konec_igre = True
        self.pristej_tocke()
        self.pocisti_polje()
        self.poberi_var.set('')
        self.besedilo['text'] = ''
        self.napotezi.set('')

    def prikazi_karte(self):  # prikaže slike kart v igralčevi roki
        for i in range(len(self.igra.human.roka)):
            slika_karte = self.igra.human.roka[i].image
            self.igralec_roka[i] = self.igralec_can.create_image(5 + i*84, 5, image=slika_karte, anchor='nw')

    def hrbtna_stran(self):  # prikaže hrbtne strani kart v nasprotnikovi roki
        for i in range(len(self.igra.pc.roka)):
            self.nasprotnik_roka[i] = self.nasprotnik_can.create_image(5 + i*84, 5, image=back, anchor='nw')

    def sprazni_kanvas_igralec(self):  # zbriše vse slike v igralčevi roki
        for i in self.igralec_roka:
            self.igralec_can.delete(i)

    def sprazni_kanvas_nasprotnik(self):
        for i in self.nasprotnik_roka:
            self.nasprotnik_can.delete(i)

    def human_igraj(self, event):  # prepozna igralčevo izbiro karte
        indeks = event.x//89
        if indeks < len(self.igra.human.roka):
            self.human_izbira.set(event.x//89)

    def igraj_na_polje(self, i, karta):  # prikaže sliko igrane karte na polju, clovek igra desno, računalnik levo
        self.igrane_karte[i] = self.igralnopolje.create_image(48 + i*84, 5, image=karta.image, anchor='nw')

    def pocisti_polje(self):  # sprazni polje
        for i in self.igrane_karte:
            self.igralnopolje.delete(i)

    def pristej_tocke(self, event=False):
        human, pc = self.humantocke_var.get(), self.pctocke_var.get()
        humanz, pcz = self.human_zmagal.get(), self.pc_zmagal.get()
        if event:
            if self.konec_igre:
                pass
            else:
                self.pc_zmagal.set(pcz+1)
                self.zmagal = -1
        else:
            if human > pc:
                self.human_zmagal.set(humanz+1)
                self.zmagal = 1
            elif human < pc:
                self.pc_zmagal.set(pcz+1)
                self.zmagal = -1
            else:
                self.human_zmagal.set(humanz+1)
                self.pc_zmagal.set(pcz+1)
                self.zmagal = 0

    def runda(self, event=False):
        self.nadaljuj.set('nadaljuj')

    def navodila(self):  # navodila
        with open('navodila.txt', 'r', encoding='utf-8') as n:
            pomoc = n.read()
        self.pomoc_win = Toplevel()
        self.pomoc_win.title('Navodila')
        lab = Label(self.pomoc_win, text=pomoc, justify=LEFT)
        lab.grid()

root = Tk()
root.title('Briškola')

back = PhotoImage(file='slike/deck.gif')
briskola = Briskola(root)

menu_main = Menu(root)
menu_submain = Menu(menu_main)
menu_main.add_cascade(label='Igra', menu=menu_submain)
menu = Menu(menu_submain)
menu_submain.add_cascade(label='Igraj', menu=menu)
menu.add_command(label='Človek vs. Računalnik', command=lambda: briskola.ustvari(False, True))
menu.add_command(label='Računalnik vs. Računalnik', command=lambda: briskola.ustvari(True, True))
menu_submain.add_command(label='Navodila', command=briskola.navodila)
root.config(menu=menu_main)

root.mainloop()