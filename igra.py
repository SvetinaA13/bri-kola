import random
from tkinter import *


def fact(n):
    if n == 0:
        return 1
    else:
        return n * fact(n-1)


def binom(n, k):
    if n < k:
        return 0
    return fact(n) / (fact(k) * fact(n-k))


def moznosti(stevec, sez, roka):
    k = len(sez)
    s = stevec
    st = 0
    for i in range(roka):
        st += binom(s, i+1) * binom(k-s, 2-i)
    return st / binom(k, roka)


class Igra:
    def __init__(self, kup, polje=None, pc=False):
        self.polje = polje
        self.kup = kup
        self.pc = Pc(self)
        self.pcvpc = pc  # 2 pc-ja oz. pc proti človeku
        if self.pcvpc:
            self.human = Pc(self)
        else:
            self.human = Human(self)
        self.pc.name = 'nasprotnik'
        self.human.name = 'igralec'
        pr = [self.pc, self.human]
        random.shuffle(pr)
        self.prvi = pr[0]
        self.drugi = pr[1]
        self.briskola = ''
        self.gone = []  # shranjuje karte ki so šle iz igre

    def runda(self):
        if self.pcvpc:
            self.polje.igralnopolje.wait_variable(self.polje.nadaljuj)
        k0 = self.prvi.igraj()
        self.polje.napotezi.set(self.drugi.name)
        if self.pcvpc:
            self.polje.igralnopolje.wait_variable(self.polje.nadaljuj)
        k1 = self.drugi.igraj(k0)
        if self.pcvpc:
            self.polje.igralnopolje.wait_variable(self.polje.nadaljuj)
        self.gone += [k0, k1]
        if k0 > k1:
            self.prvi.dodaj_na_kup(k0)
            self.prvi.dodaj_na_kup(k1)
            self.polje.poberi_var.set('Poberi')  # pozove igralca naj pobere karto s kupa
            self.prvi.poberi_s_kupa(self.kup)
            self.drugi.poberi_s_kupa(self.kup)
        else:
            self.drugi.dodaj_na_kup(k0)
            self.drugi.dodaj_na_kup(k1)
            self.polje.poberi_var.set('Poberi')  # pozove igralca naj pobere karto s kupa
            self.drugi.poberi_s_kupa(self.kup)
            self.prvi.poberi_s_kupa(self.kup)
            self.prvi, self.drugi = self.drugi, self.prvi
        self.polje.humantocke_var.set(self.human.sestej())
        self.polje.pctocke_var.set(self.pc.sestej())

        self.human.izbira = False  # človek
        self.polje.napotezi.set(self.prvi.name)
        self.polje.sprazni_kanvas_igralec()
        self.polje.sprazni_kanvas_nasprotnik()
        self.polje.pocisti_polje()
        self.polje.prikazi_karte()
        self.polje.hrbtna_stran()
        if len(self.kup) == 0:
            self.polje.kupcek.delete(self.polje.kup)
            self.polje.briskola_can.delete(self.polje.br)


class Karta:
    def __init__(self, barva, stevilo, tocke):
        self.b = barva
        self.st = stevilo
        self.tc = tocke
        self.bris = False
        niz = 'slike/' + self.b + str(self.st) + '.gif'
        self.image = PhotoImage(file=niz)

    def __lt__(self, other):
        if self.b == other.b:
            if self.tc < other.tc:
                return True
            else:
                if self.tc == other.tc:
                    return self.st < other.st
                return False
        else:
            if self.bris or other.bris:
                return self.bris < other.bris
            return False

    def __gt__(self, other):
        return not self < other

    def __repr__(self):
        b = self.b
        st = self.st
        return str(b) + ' ' + str(st)

    def __str__(self):
        b = self.b
        st = self.st
        return str(b) + ' ' + str(st)

    def je_briskula(self):
        self.bris = True


class Player:
    def __init__(self, igra):
        self.roka = []
        self.kupcek = []
        self.igra = igra
        self.name = ''

    def dodaj_na_kup(self, card):  # doda izbrano karto na kup
        self.kupcek += [card]

    def sestej(self):  # prešteje točke kart v kupčku
        vsota = 0
        for i in self.kupcek:
            vsota += i.tc
        return vsota

    def prob(self, card):  # pove kolikšna je možnost, da nasprotnik pobere karto
        if card.bris:
            return float('inf')  # če je karta briškula, je ne štejemo
        c = self.igra.drugi.roka + self.igra.kup
        stevec = 0
        for i in c:  # prešteje močnejše karte
            if i > card:
                stevec += 1
        return moznosti(stevec, c, len(self.roka))


class Human(Player):
    def __init__(self, igra):
        Player.__init__(self, igra)
        self.izbira = False

    def poberi(self, event):  # nastavi spremenljivko poberi_var na prazen niz in s tem sproži metodo poberi_s_kupa
        if self.igra.polje.poberi_var.get():  # if stavek prepreči izvedbo metode ob nepravem času
            self.igra.polje.poberi_var.set('')

    def __repr__(self):
        return 'Human(' + str(self.roka)+')'

    def poberi_s_kupa(self, kup):
        self.igra.polje.igralnopolje.wait_variable(self.igra.polje.poberi_var)
        if len(kup) != 0:
            self.roka += [kup.pop()]

    def igraj(self, k0=False):
        self.igra.polje.igralnopolje.wait_variable(self.igra.polje.human_izbira)  # počaka da igralec klikne na karto
        indeks = self.igra.polje.human_izbira.get()
        izb = self.roka.pop(indeks)
        self.igra.polje.igralec_can.delete(self.igra.polje.igralec_roka[indeks])
        self.igra.polje.igraj_na_polje(1, izb)
        return izb


class Pc(Player):
    def __init__(self, igra):
        Player.__init__(self, igra)

    def __repr__(self):
        return 'PC(' + str(self.roka)+')'

    def poberi_s_kupa(self, kup):
        if len(kup) != 0:
            self.roka += [kup.pop()]

    def igraj(self, k0=False):
        # racunalnik pogleda s katero karto izgubi najmanj točk (če je na potezi) oz. največ točk pridobi (če je drugi)
        kandidati = list()
        if not k0:
            for card in self.roka:
                kandidati += [(-card.tc, not card.bris)]
            i = 0
            profit = float('-inf')
            for j, k in enumerate(kandidati):
                a, b = k
                if a + b > profit:
                    i = j
                    profit = a + b
            if self.roka[i].bris:
                # računalnik izračuna za katero karto je najmanj možnosti, da jo nasprotnik pobere
                dic = dict([(self.prob(self.roka[i]), i) for i in range(len(self.roka))])
                if min(dic.keys()) < 0.5:
                    i = dic[min(dic.keys())]
        else:
            # naredi seznam profitov/izgub
            for card in self.roka:
                if k0 > card:
                    kandidati += [-(k0.tc + card.tc)]
                else:
                    if card.bris:
                        kandidati += [k0.tc - card.tc]
                    else:
                        kandidati += [k0.tc + card.tc]
            i = 0
            profit = float('-inf')
            for j, k in enumerate(kandidati):
                if k > profit:
                    i = j
                    profit = k
        izbira = self.roka.pop(i)
        if self == self.igra.pc:
            self.igra.polje.igraj_na_polje(0, izbira)
            self.igra.polje.nasprotnik_can.delete(self.igra.polje.nasprotnik_roka[i])
        else:
            self.igra.polje.igralec_can.delete(self.igra.polje.igralec_roka[i])
            self.igra.polje.igraj_na_polje(1, izbira)
        return izbira

# ####################### ZMESA KUPCEK ###############################


def ustvari_igro(p):  # vrne pripravljeno igro in slovar v katerem so photoimage objekti za vse karte
    barve = list(('spada', 'kopa', 'baston', 'denar'))
    stevilke = list()
    for i in list((2, 4, 5, 6, 7)):
        stevilke += [[i, 0]]

    stevilke += [[1, 11], [3, 10], [11, 2], [12, 3], [13, 4]]

    stevilke.sort()

    karte = list()
    for i in barve:
        for j in stevilke:
            k, l = j
            karte += [Karta(i, k, l)]

    s = [i for i in range(40)]
    random.shuffle(s)

    # v spremenljivki d je shranjen seznam kart - kupcek
    d = list()
    for i in s:
        d += [karte[i]]

    # briskula je adut igre
    briskula = d.pop()
    briskula.je_briskula()

    for card in d:
        if card.b == briskula.b:
            card.je_briskula()

# ########################### PRIPRAVI IGRALCA ############################

    igra = Igra(d, pc=p)
    igra.briskola = briskula

    for i in range(3):
        igra.prvi.roka += [d.pop()]
        igra.drugi.roka += [d.pop()]

    igra.kup = [briskula] + d

    return igra